﻿// © 2018 adrian.licensing@gmail.com This Software is made available under the MIT License.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MathPrimer
{
    public class RayCaster : MonoBehaviour
    {
        public float amplitudeX = 1f;
        public float amplitudeY = 1f;

        public float oscillationPeriodFactor = 1f;

        public bool enableShrinking;
        public float laserEnergy = 1f;


        public List<SphereCollider> spheres = new List<SphereCollider>();
        public bool isHit;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (enableShrinking)
            {
                Ray ray = CalcRay();

                RaycastHit hitInfo = RaycastGeometrical(ray, out hitInfo);

                //RaycastHit hitInfo = RaycastAnalytical(ray, out hitInfo);

                if (isHit)
                {
                    Transform hitTransform = hitInfo.collider.transform;
                    hitTransform.localScale -= 0.1f * Time.deltaTime * Vector3.one * laserEnergy;
                    hitTransform.localScale = Vector3.Max(hitTransform.localScale, Vector3.zero);
                }
            }
        }

        /// <summary>     
        /// For each sphere finds out whether the ray intersects with it and on the base of distance from origin to the point of intersection finds the smallest one and returns RaycastHit object containing info.
        /// </summary>
        /// <returns></returns>
        RaycastHit RaycastGeometrical(Ray ray, out RaycastHit info)
        {           
            info = new RaycastHit();
            float distsMin = int.MaxValue;
            isHit = false;
            for (int i = 0; i < spheres.Count(); ++i)
            {
                Vector3 center = spheres[i].transform.TransformPoint(spheres[i].center);
                float radius = spheres[i].radius * spheres[i].transform.lossyScale.x; // assuming global scale in uniform
                Vector3 closest = ray.origin + ray.direction * Vector3.Dot(ray.direction, center - ray.origin);
                float distToLine = Vector3.Distance(closest, center);
                if (distToLine < radius)
                {                  
                    float distToPoint = Mathf.Sqrt(radius * radius - distToLine * distToLine);
                    Vector3 entry = closest - distToPoint * ray.direction;                
                    if (i == 0 || Vector3.Distance(ray.origin, entry) < distsMin)
                    {
                        distsMin = Vector3.Distance(ray.origin, entry);
                        isHit = true;
                        info.point = entry;
                        info.normal = (entry - center).normalized;
                    }
                }
               
            }
            return info;
        }



        RaycastHit RaycastAnalytical(Ray ray, out RaycastHit info)
        {
            info = new RaycastHit();
            float distsMin = int.MaxValue;
            isHit = false;
            for (int i = 0; i < spheres.Count(); ++i)
            {
                Vector3 center = spheres[i].transform.TransformPoint(spheres[i].center);
                float radius = spheres[i].radius * spheres[i].transform.lossyScale.x; // assuming global scale in uniform
                Vector3 distOriginCenter = ray.origin - center;
                float b = 2 * Vector3.Dot(distOriginCenter, ray.direction);
                float c = distOriginCenter.magnitude * distOriginCenter.magnitude - radius*radius;
                float bb4ac = b * b - 4 * c;
                if ( bb4ac > 0 )
                {
                    float sqrtDiscr = Mathf.Sqrt(bb4ac);
                    float t0 = (-b - sqrtDiscr) / 2;
                    float t1 = (-b + sqrtDiscr) / 2;
                    Vector3 entry = (t0 > 0) ? ray.origin + t0 * ray.direction : ray.origin + t1 * ray.direction;
                    if (i == 0 || Vector3.Distance(ray.origin, entry) < distsMin)
                    {
                        distsMin = Vector3.Distance(ray.origin, entry);
                        isHit = true;
                        info.point = entry;
                        info.normal = (entry - center).normalized;
                    }
                }
            }
            return info; 
        }

        private void OnDrawGizmos()
        {
            Ray ray = CalcRay();

            RaycastHit hitInfo = RaycastAnalytical(ray, out hitInfo);

            //RaycastHit hitInfo = RaycastGeometric(ray, out hitInfo);

            if (isHit)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(ray.origin, hitInfo.point);
                Gizmos.DrawWireSphere(hitInfo.point, 0.1f);
            }
            else
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(ray.origin, ray.origin + 100f * ray.direction);
                Debug.Log("I did not hit her, it`s not true.");
            }
        }

        private Ray CalcRay()
        {
            float angleY = Mathf.Sin(Time.time * 2f * Mathf.PI / 1f / oscillationPeriodFactor) * amplitudeY;
            float angleX = Mathf.Sin(Time.time * 2f * Mathf.PI / 7f / oscillationPeriodFactor) * amplitudeX;

            // Matrix3x3 rotation = Matrix3x3.Euler(angleX, angleY, 0f);
            Quaternion rayRotation = Quaternion.Euler(angleX, angleY, 0f);

            Vector3 rayDirectionLocal = rayRotation * Vector3.forward;
            //transform.rotation * rayDirectionLocal;
            Vector3 rayDirectionInWorld = transform.TransformDirection(rayDirectionLocal);

            // 

            Vector3 origin = transform.position;
            Vector3 direction = rayDirectionInWorld;

            Ray ray = new Ray(origin, direction);
            return ray;
        }
    }
}
