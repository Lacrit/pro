package observer;

public interface IListener<T> {

    public void registerEvent(T data);

}
