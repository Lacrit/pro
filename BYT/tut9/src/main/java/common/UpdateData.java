package common;

import java.util.Date;

public class UpdateData {

    private Date updateTime;

    public UpdateData(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public String toString() {
        return "main.java.common.UpdateData{" +
                "updateTime=" + updateTime +
                '}';
    }
}
