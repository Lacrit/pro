package memento;

import java.util.Objects;

public abstract class Memento<T> {

    private T state;

    public Memento(T state) {
        this.state = state;
    }

    public T getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Memento)) return false;
        Memento<?> memento = (Memento<?>) o;
        return Objects.equals(state, memento.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(state);
    }
}
