package memento;


import common.UpdateData;
import observer.IListener;

import java.net.URLConnection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;


public class WebUpdateManagerState {

    private HashMap<String, LinkedList<IListener<UpdateData>>> listenerMap;
    private HashMap<String, URLConnection> connectionMap;
    private HashMap<String, UpdateData> updateMap;

    public WebUpdateManagerState(HashMap<String, LinkedList<IListener<UpdateData>>> listenerMap, HashMap<String, URLConnection> connectionMap, HashMap<String, UpdateData> updateMap) {
        this.listenerMap = new HashMap<>(listenerMap);
        this.connectionMap = new HashMap<>(connectionMap);
        this.updateMap = new HashMap<>(updateMap);
    }

    public HashMap<String, LinkedList<IListener<UpdateData>>> getListenerMap() {
        return listenerMap;
    }

    public void setListenerMap(HashMap<String, LinkedList<IListener<UpdateData>>> listenerMap) {
        this.listenerMap = listenerMap;
    }

    public HashMap<String, URLConnection> getConnectionMap() {
        return connectionMap;
    }

    public void setConnectionMap(HashMap<String, URLConnection> connectionMap) {
        this.connectionMap = connectionMap;
    }

    public HashMap<String, UpdateData> getUpdateMap() {
        return updateMap;
    }

    public void setUpdateMap(HashMap<String, UpdateData> updateMap) {
        this.updateMap = updateMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WebUpdateManagerState)) return false;
        WebUpdateManagerState that = (WebUpdateManagerState) o;
        return Objects.equals(listenerMap, that.listenerMap) &&
                Objects.equals(connectionMap, that.connectionMap) &&
                Objects.equals(updateMap, that.updateMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(listenerMap, connectionMap, updateMap);
    }
}
