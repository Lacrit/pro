package b_Money;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CurrencyTest {
	Currency SEK, DKK, NOK, EUR;
	
	@Before
	public void setUp() throws Exception {
		/* Setup currencies with exchange rates */
		SEK = new Currency("SEK", 0.15);
		DKK = new Currency("DKK", 0.20);
		EUR = new Currency("EUR", 1.5);
	}

	@Test
	public void testGetName() { // why pietia, why 
		assertEquals("SEK", SEK.getName());
		assertEquals("DKK", DKK.getName());
		assertEquals("EUR", EUR.getName());
	}
	
	@Test
	public void testGetRate() { // why is this happening to me 
		assertEquals(0.15, SEK.getRate(), 0.0);
		assertEquals(0.2, DKK.getRate(), 0.0);
		assertEquals(1.5, EUR.getRate(), 0.0);
	}
	
	@Test
	public void testSetRate() {
		SEK.setRate(0.2);
		assertEquals(0.2, SEK.getRate(), 0.0);

		DKK.setRate(0.25);
		assertEquals(0.25, DKK.getRate(), 0.0);

		EUR.setRate(1.55);
		assertEquals(1.55, EUR.getRate(), 0.0);
	}
	
	@Test
	public void testGlobalValue() {
		assertEquals(30, SEK.universalValue(200), 0);
		assertEquals(60, DKK.universalValue(300), 0);
		assertEquals(600, EUR.universalValue(400), 0);
	}
	
	@Test
	public void testValueInThisCurrency() {
		assertEquals(100, SEK.valueInThisCurrency(10, EUR), 0);
		assertEquals(20, EUR.valueInThisCurrency(200, SEK), 0);
		assertEquals(0, SEK.valueInThisCurrency(0, EUR), 0);
		assertEquals(-10, EUR.valueInThisCurrency(-100, SEK), 0);
		assertEquals(100, SEK.valueInThisCurrency(100, SEK), 0);
		assertEquals(200, EUR.valueInThisCurrency(200, EUR), 0);
	}

}
