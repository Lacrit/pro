package classes;

import java.util.LinkedList;
import java.util.List;

import Exceptions.InvalidNameOrSurnameException;

public class Specialization {

	private String name; 
	private String description; 
	private static List<Specialization> specializations = new LinkedList<>();
	
	public Specialization(String name, String description) throws InvalidNameOrSurnameException {
		setName(name);
		setDescription(description);
		specializations.add(this);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) throws InvalidNameOrSurnameException {
		if (!isValid(name)) throw new InvalidNameOrSurnameException();
		this.name = name;
	}

	private boolean isValid(String name){
		return name.length() > 1;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static List<Specialization> listAllSpecializations() {
		for(Specialization specialization: specializations)
	    {
			System.out.println(specialization);
	    }
	    return specializations;
	}

}
