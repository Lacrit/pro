package classes;

import java.time.LocalTime;

import javax.management.InvalidAttributeValueException;

public class Shift {

	private LocalTime start;
	private LocalTime end;
	
	public Shift(LocalTime start, LocalTime end) {
	    setEnd(end);
	}

	public LocalTime getStart() {
		return start;
	}

	public void setStart(LocalTime start) {
		this.start = start;
	}

	public LocalTime getEnd() {
		return end;
	}

	public void setEnd(LocalTime end) {
		this.end = end;
	}

}
