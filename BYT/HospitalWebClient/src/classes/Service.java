package classes;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import Exceptions.PriceLowerThanZeroException;

public abstract class Service {

	private BigDecimal price; 
	private int duration;
	private String description; 
	private LocalDateTime start;
	private LocalDateTime end; 
	private static List<Service> services = new LinkedList<>();
	
	public Service(LocalDateTime start, BigDecimal price, int duration, String description) throws PriceLowerThanZeroException {
		 setStart(start);
		 setDuration(duration);
		 setEnd(getStart().plusHours(duration));
		 setPrice(price);
		 setDescription(description);
		 services.add(this);
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}
	
	public void setPrice(BigDecimal price) throws PriceLowerThanZeroException { 
		if (!price.abs().equals(price)) throw new PriceLowerThanZeroException();
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static List<Service> getServices() {
		return services;
	}

	public static void setServices(List<Service> services) {
		Service.services = services;
	}
	
	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public static List<Service> listAllServices() {
		for(Service service: services)
	    {
			System.out.println(service);
	    }
	    return services;
	}
}
