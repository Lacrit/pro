package tests;

import classes.Administrator;
import org.junit.Assert;
import org.junit.Before;

import java.time.LocalDate;

public class AdministratorTest {

    private Administrator mockAdministrator;

    @Before
    public void setUp() throws Exception {
        mockAdministrator = new Administrator("Name", "Surname", " 456456456", "AB", LocalDate.now(),
                Administrator.privilege.REQUEST_REVIEW);
    }

    @org.junit.Test
    public void getPrivilege() {
        Assert.assertEquals(Administrator.privilege.REQUEST_REVIEW, mockAdministrator.getPrivilege());

    }

    @org.junit.Test
    public void setPrivilege() {
        Administrator.privilege oldPriv = mockAdministrator.getPrivilege();
        mockAdministrator.setPrivilege(Administrator.privilege.NONE);
        Assert.assertNotEquals(mockAdministrator.getPrivilege(), oldPriv);
    }
}