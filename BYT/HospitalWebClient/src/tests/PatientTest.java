package tests;

import Exceptions.PriceLowerThanZeroException;
import classes.Patient;
import classes.Payment;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class PatientTest {

    private Patient mockPatient;

    @Before
    public void setUp() throws Exception {
        mockPatient = new Patient("PName", "PSname", "766756756", "O", LocalDate.now());
    }

    @Test
    public void getMedicalDetails() {
        mockPatient.setMedicalDetails("no details");
        Assert.assertEquals("no details" , mockPatient.getMedicalDetails());
    }

    @Test
    public void setMedicalDetails() {
        getMedicalDetails();
    }

    @Test
    public void getPayments() {
        ArrayList<Payment> payments = new ArrayList<>();
        try {
            payments.add(new Payment(LocalDate.now(), LocalTime.now(), new BigDecimal(500), Payment.payment.CARD));
        } catch (PriceLowerThanZeroException e) {
            e.printStackTrace();
        }
        mockPatient.setPayments(payments);
        Assert.assertEquals(1, mockPatient.getPayments().size());
    }

    @Test
    public void setPayments() {
        getPayments();
    }
}