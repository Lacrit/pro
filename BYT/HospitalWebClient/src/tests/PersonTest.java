package tests;

import Exceptions.InvalidBirthDateException;
import Exceptions.InvalidBloodTypeException;
import Exceptions.InvalidNameOrSurnameException;
import classes.Administrator;
import classes.Doctor;
import classes.Patient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class PersonTest {

    private Administrator mockAdministrator;
    private Doctor mockDoc;
    private Patient mockPatient;

    @Before
    public void setUp() throws Exception {
        mockAdministrator = new Administrator("Name", "Surname", " 456456456", "AB", LocalDate.now(),
                Administrator.privilege.REQUEST_REVIEW);
        mockDoc = new Doctor("DocName", "DocSname", "567567109", "B", LocalDate.now());
        mockPatient = new Patient("PName", "PSname", "766756756", "O", LocalDate.now());
    }

    @Test
    public void getName() {
        setName();
    }

    @Test
    public void getPhoneNum() {
       setPhoneNum();
    }

    @Test
    public void getBloodType() {
        setBloodType();
    }

    @Test
    public void getBirthdate() {
        setBirthdate();
    }

    @Test
    public void getSurname() {
        setSurname();
    }

    @Test
    public void setBloodType() {
        String badBType = "AABB";
        try {
            mockPatient.setBloodType(badBType);
        } catch (InvalidBloodTypeException e) {
            Assert.assertTrue(true);
            e.printStackTrace();
        }
        Assert.assertFalse(mockPatient.getBloodType().equals(badBType));
    }

    @Test
    public void setPhoneNum() {
        String badPhoneNum = "4345gf";
        mockAdministrator.setPhoneNum(badPhoneNum);
        assertNotEquals(mockAdministrator.getPhoneNum(), badPhoneNum);
    }

    @Test
    public void setBirthdate() {
        LocalDate badDate = LocalDate.of(2020,10,30);
        try {
            mockPatient.setBirthdate(badDate);
        } catch (InvalidBirthDateException e) {
            e.printStackTrace();
            Assert.assertTrue(true);
        }
        Assert.assertFalse(mockPatient.getBirthdate().equals(badDate));
    }

    @Test
    public void setName() {
        try {
            mockDoc.setName("");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidNameOrSurnameException);
            e.printStackTrace();
        }
        Assert.assertFalse(mockDoc.getName().equals(""));
    }

    @Test
    public void setSurname() {
        try {
            mockDoc.setSurname("");
        } catch (Exception e) {
            Assert.assertTrue(e instanceof InvalidNameOrSurnameException);
            e.printStackTrace();
        }
        Assert.assertFalse(mockDoc.getSurname().equals(""));
    }
}