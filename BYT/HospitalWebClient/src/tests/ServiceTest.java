package tests;

import Exceptions.PriceLowerThanZeroException;
import classes.Procedure;
import classes.Service;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class ServiceTest {

    private Service mockService;

    @Before
    public void setUp() throws Exception {
        mockService = new Procedure(LocalDateTime.now(),new BigDecimal(1000),2, "Surgery", "120A");
    }

    @Test
    public void setPrice() {
        BigDecimal negativeAmount = new BigDecimal(-1000);
        try {
            mockService.setPrice(negativeAmount);
        } catch (PriceLowerThanZeroException e) {
            assertTrue(true);
            e.printStackTrace();
        }
        assertFalse(mockService.getPrice().equals(negativeAmount));
    }

}