package tests;

import Exceptions.InvalidNameOrSurnameException;
import classes.Doctor;
import classes.Shift;
import classes.Specialization;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class DoctorTest {

    private Doctor mockDoc;

    @Before
    public void setUp() throws Exception {
        mockDoc = new Doctor("DocName", "DocSname", "567567109", "B", LocalDate.now());
    }

    @Test
    public void getTimetable() {
        HashMap<Integer, Shift> timetable = new HashMap<Integer, Shift>();
        Shift dayShift = new Shift(LocalTime.NOON, LocalTime.NOON.plusHours(8));
        Shift nightShift = new Shift(LocalTime.MIDNIGHT.minusHours(4), LocalTime.MIDNIGHT.plusHours(4));
        timetable.put(0, dayShift);
        timetable.put(2, nightShift);
        timetable.put(6, nightShift);

        mockDoc.setTimetable(timetable);
        Assert.assertTrue(mockDoc.getTimetable().entrySet().size() == 3);
    }

    @Test
    public void setTimetable() {
        getTimetable();
    }

    @Test
    public void getSpecializations() {
        setSpecializations();
    }

    @Test
    public void setSpecializations() {
        try {
            Specialization mockSpec = new Specialization("Spec1", "General doctor");
            Specialization mockSpec2 = new Specialization("Spec2", "Immunologist");
            mockDoc.getSpecializations().add(mockSpec);
            mockDoc.getSpecializations().add(mockSpec2);
            Assert.assertTrue(mockDoc.getSpecializations().size() == 2);
            ArrayList<Specialization> newSpecList = new ArrayList<>();
            newSpecList.add(mockSpec);
            mockDoc.setSpecializations(newSpecList);
            Assert.assertTrue(mockDoc.getSpecializations().size() == 1);

        } catch (InvalidNameOrSurnameException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
    }
}