package Exceptions;

public class InvalidNameOrSurnameException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4830849447612756502L;

	public InvalidNameOrSurnameException() {
        super();
    }
}